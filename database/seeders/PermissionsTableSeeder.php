<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'id' => '1',
                'title' => 'loan_application_create',
            ],
            [
                'id' => '2',
                'title' => 'loan_application_edit',
            ],
            [
                'id' => '3',
                'title' => 'loan_application_show',
            ],
            [
                'id' => '4',
                'title' => 'loan_application_delete',
            ],
            [
                'id' => '5',
                'title' => 'loan_application_access',
            ]
        ];

        Permission::insert($permissions);
    }
}
