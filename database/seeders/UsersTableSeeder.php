<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'id' => 1,
                'first_name' => 'Admin',
                'last_name' => 'Admin',
                'email' => 'admin@admin.com',
                'phone' => '1234567890',
                'country_code' => '+1',
                'password' => '$2y$10$PadOOF6GiHJqI1IQhPZNjeXkKGPip9vJXdhB5ra6lrvZdcZFZDCjy', //password
                'remember_token' => null,
            ],
            [
                'id' => 2,
                'first_name' => 'Dhananjay',
                'last_name' => 'Kyada',
                'email' => 'dhananjaykyada@gmail.com',
                'phone' => '9876543210',
                'country_code' => '+91',
                'password' => '$2y$10$PadOOF6GiHJqI1IQhPZNjeXkKGPip9vJXdhB5ra6lrvZdcZFZDCjy', //password
                'remember_token' => null,
            ],
        ];

        User::insert($users);
    }
}
