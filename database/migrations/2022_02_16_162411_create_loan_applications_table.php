<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_applications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->decimal('amount', 15, 2);
            $table->decimal('amount_paid', 15, 2)->default(0);
            $table->decimal('emi', 15, 2)->default(0);
            $table->decimal('amount_remaining', 15, 2)->default(0);
            $table->decimal('total_amount_with_interest', 15, 2)->default(0);
            $table->decimal('interest', 10, 2);
            $table->integer('duration');
            $table->unsignedInteger('installments_total')->default(0);
            $table->unsignedInteger('installments_paid')->default(0);
            $table->unsignedInteger('installments_remaining')->default(0);
            $table->dateTime('date_applied');
            $table->dateTime('date_loan_ends');
            $table->dateTime('date_completed')->nullable();
            $table->dateTime('next_date')->nullable();
            $table->unsignedBigInteger('approved_by')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_applications');
    }
}
