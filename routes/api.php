<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController as ApiAuthController;
use App\Http\Controllers\API\LoanApplicationController as ApiLoanApplicationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// prefix api with v1
Route::prefix('v1')->group(function () {
    Route::group(['namespace' => 'API'], function () {

        // prefix auth api
        Route::group(['prefix' => 'auth'], function () {
            //login api
            Route::post('login', [ApiAuthController::class, 'login'])->name('login');

            //signup api
            Route::post('signup', [ApiAuthController::class, 'signup'])->name('signup');

            // route group with authorization
            Route::group(['middleware' => 'auth:sanctum'], function () {
                // logout
                Route::post('logout', [ApiAuthController::class, 'logout'])->name('logout');

                // get profile data
                Route::post('get_profile_data', [ApiAuthController::class, 'getUserProfileData'])->name('get_profile_data');
                //update profile data
                Route::post('update_profile', [ApiAuthController::class, 'updateProfile'])->name('update_profile');
                // change password
                Route::post('change_password', [ApiAuthController::class, 'changePassword'])->name('change_password');
                // add device token
                Route::post('add_device_token', [ApiAuthController::class, 'addDeviceToken'])->name('add_device_token');
            });
        });

        Route::group(['middleware' => 'auth:sanctum'], function () {
            /*********** only normal user can access *******/
            // apply new loan
            Route::post('apply_loan', [ApiLoanApplicationController::class, 'applyLoan'])->name('apply_loan');
            // get loan data with repayments
            Route::post('get_loan_data', [ApiLoanApplicationController::class, 'getLoanData'])->name('get_loan_data');

            /*********** only admin user can access *******/
            // approve loan
            Route::post('approve_loan', [ApiLoanApplicationController::class, 'approveLoan'])->name('approve_loan');
            // pay loan
            Route::post('pay_loan', [ApiLoanApplicationController::class, 'payLoan'])->name('pay_loan');
            // get all loan data
            Route::post('get_all_loan_data', [ApiLoanApplicationController::class, 'getAllLoanData'])->name('get_all_loan_data');
        });
    });
});
