<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */

    public function sendResponse($message, $result)
    {
        $response = [
            'status' => 1,
            'message' => $message,
            'data' => $result,
        ];

        return response()->json($response, 200);
    }

    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */

    public function sendError($message)
    {
        $response = [
            'status' => 0,
            'message' => $message,
        ];

        return response()->json($response, 200);
    }

    /**
     * return validation error response.
     *
     * @return \Illuminate\Http\Response
     */

    public function sendValidationError($message, $errorData)
    {
        $response = [
            'status' => 2,
            'message' => $message,
            'data' => $errorData,
        ];

        return response()->json($response, 200);
    }
}
