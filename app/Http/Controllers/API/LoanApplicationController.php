<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\LoanApplication;
use App\Models\LoanRepayment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LoanApplicationController extends BaseController
{
    // apply new loan
    public function applyLoan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'interest' => 'required|numeric',
            'duration' => 'required|integer',
            'start_date' => 'required|date',
        ]);

        if ($validator->fails()) {
            return $this->sendValidationError(__('Validation Error'), $validator->errors());
        }

        $user = $request->user();

        //check if user has already a loan applied or in progress
        $exists_loan = LoanApplication::where('user_id', $user->id)->whereIn('status', [LOAN_STATUS_APPROVED, LOAN_STATUS_NEW])->first();
        if ($exists_loan)
            return $this->sendError('You have one loan to be completed first.');

        $loan_start_date = Carbon::createFromFormat('Y-m-d', $request->start_date);
        $loan_end_date = Carbon::createFromFormat('Y-m-d', $request->start_date)->addWeek($request->duration);
        $loan_application = new LoanApplication();
        $loan_application->user_id = $user->id;
        $loan_application->amount = $request->amount;
        $loan_application->interest = $request->interest;
        $loan_application->duration = $request->duration;
        $loan_application->date_applied = $loan_start_date->format('Y-m-d H:i:s');
        $loan_application->date_loan_ends = $loan_end_date->format('Y-m-d H:i:s');
        $loan_application->amount_remaining = $request->amount;
        $loan_application->amount_paid = 0;
        $loan_application->installments_total = $request->duration;
        $loan_application->installments_paid = 0;
        $loan_application->installments_remaining = $request->duration;
        $loan_application->status = LOAN_STATUS_NEW;
        if ($loan_application->save()) {
            $emi = $this->calculateEMIAmount($loan_application);
            $loan_application->emi = number_format((float)$emi, 2, '.', '');
            $loan_application->total_amount_with_interest = number_format((float)$emi * $loan_application->duration, 2, '.', '');
            $loan_application->save();
            $loan_application_model = new LoanApplication();
            $data = $loan_application_model->getSingleLoanApplicationData($loan_application);
            return $this->sendResponse('Your Loan Application Submitted Successfully.', $data);
        }

        return $this->sendError('Your Loan Application Not Submitted.');
    }

    // get user's all loan data
    public function getLoanData(Request $request)
    {
        $data = [];
        $user = $request->user();
        $loan_applications = LoanApplication::with('loan_repayments')->where('user_id', $user->id)->get();

        if ($loan_applications) {
            $loan_data = [];
            $loan_application_model = new LoanApplication();
            foreach ($loan_applications as $loan_application) {
                $loan_data = $loan_application_model->getSingleLoanApplicationData($loan_application);
                $loan_repayments = [];
                if ($loan_application->loan_repayments->count() > 0) {
                    $loan_repayment_model = new LoanRepayment();
                    foreach ($loan_application->loan_repayments as $loan_repayment) {
                        $loan_repayments[] = $loan_repayment_model->getSingleLoanRepaymentData($loan_repayment);
                    }
                }

                $loan_data['loan_repayments'] = $loan_repayments;

                $data[] = $loan_data;
            }

            return $this->sendResponse('Loan Application Data Loaded Successfully.', $data);
        }

        return $this->sendError('No Loan Application Found.');

    }


    // get all loan data
    public function getAllLoanData(Request $request)
    {
        $data = [];

        // check if user has permission
        if (Gate::denies('loan_application_access')) {
            return $this->sendError('You are not authorized.');
        }

        $loan_applications = LoanApplication::with('loan_repayments')->get();
        if ($loan_applications) {
            $loan_data = [];
            $loan_application_model = new LoanApplication();
            foreach ($loan_applications as $loan_application) {
                $loan_data = $loan_application_model->getSingleLoanApplicationData($loan_application);
                $loan_repayments = [];
                if ($loan_application->loan_repayments->count() > 0) {
                    $loan_repayment_model = new LoanRepayment();
                    foreach ($loan_application->loan_repayments as $loan_repayment) {
                        $loan_repayments[] = $loan_repayment_model->getSingleLoanRepaymentData($loan_repayment);
                    }
                }

                $loan_data['loan_repayments'] = $loan_repayments;

                $data[] = $loan_data;
            }

            return $this->sendResponse('Loan Application Data Loaded Successfully.', $data);
        }

        return $this->sendError('No Loan Application Found.');

    }

    // approve new loan - only admin user can access
    public function approveLoan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'loan_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->sendValidationError(__('Validation Error'), $validator->errors());
        }

        // check if user has permission
        if (Gate::denies('loan_application_access')) {
            return $this->sendError('You are not authorized.');
        }

        $user = $request->user();

        $loan_application = LoanApplication::where('id', $request->loan_id)->where('status', LOAN_STATUS_NEW)->first();
        if ($loan_application) {
            $loan_application->status = LOAN_STATUS_APPROVED;
            $loan_application->approved_by = $user->id;
            if ($loan_application->save()) {
                $loan_application_model = new LoanApplication();
                $data = $loan_application_model->getSingleLoanApplicationData($loan_application);
                return $this->sendResponse('Loan Application Submitted Successfully.', $data);
            }
        }
        return $this->sendError('No Such Loan Application Found.');
    }

    // pay loan api - only admin user can access
    public function payLoan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'loan_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->sendValidationError(__('Validation Error'), $validator->errors());
        }

        // check if user has permission
        if (Gate::denies('loan_application_access')) {
            return $this->sendError('You are not authorized.');
        }

        $user = $request->user();

        // only application with approve status
        $loan_application = LoanApplication::where('id', $request->loan_id)->where('status', LOAN_STATUS_APPROVED)->first();

        if ($loan_application) {
            $loan_repayment = new LoanRepayment();
            $loan_repayment->loan_id = $loan_application->id;
            $loan_repayment->receiver_id = $user->id;
            $amount_to_be_paid = $loan_application->emi;
            $loan_repayment->amount = $amount_to_be_paid;
            $loan_repayment->payment_date = Carbon::now()->format('Y-m-d H:i:s');
            $interest = (($loan_application->interest / 100) * $loan_application->amount_remaining) / 52;
            $interest = number_format((float)$interest, 2, '.', '');
            $principal = $amount_to_be_paid - $interest;
            $loan_repayment->interest = $interest;
            $loan_repayment->principal = $principal;

            if ($request->notes) {
                $loan_repayment->notes = $request->notes;
            }
            if ($loan_repayment->save()) {
                $loan_application->amount_remaining = $loan_application->amount_remaining - $amount_to_be_paid;
                $loan_application->amount_paid = $loan_application->amount_paid + $amount_to_be_paid;
                $loan_application->installments_paid = $loan_application->installments_paid + 1;
                $loan_application->installments_remaining = $loan_application->installments_remaining - 1;

                if ($loan_application->installments_remaining <= 0) {
                    $loan_application->status = LOAN_STATUS_COMPLETED;
                    $loan_application->date_completed = Carbon::now()->format('Y-m-d H:i:s');
                }
                $loan_application->save();

                $loan_repayment_model = new LoanRepayment();
                $data = $loan_repayment_model->getSingleLoanRepaymentData($loan_repayment);
                return $this->sendResponse('Loan Payment Submitted Successfully.', $data);
            } else {
                return $this->sendError('Loan Payment not saved.');
            }
        }
        return $this->sendError('No Such Loan Application Found.');
    }

    // function to calculate emi based on interest and amount
    public function calculateEMIAmount($loan)
    {
        $duration = $loan->duration;
        $interest = (float)$loan->interest;
        $loan_amount = (float)$loan->amount;
        $period = $duration;
        $interest = $interest / 5200;
        $amount = $interest * -$loan_amount * pow((1 + $interest), $period) / (1 - pow((1 + $interest), $period));
        $amount = number_format((float)$amount, 2, '.', '');
        return $amount;
    }
}
