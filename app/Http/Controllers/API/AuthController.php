<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use App\Models\UserDeviceToken;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends BaseController
{
    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'phone' => 'required|string',
            'country_code' => 'required|string',
            'password' => 'required|string|confirmed',
            'password_confirmation' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->sendValidationError(__('Validation Error'), $validator->errors());
        }

        $user = new User([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'country_code' => $request->country_code,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
        ]);

        if ($user->save()) {

            $user->roles()->sync(2);
            $tokenResult = $user->createToken('Aspire Access Token');

            if ($request->device_token) {
                $is_user_token_exists = UserDeviceToken::where('user_id', $user->id)->where('device_token', $request->device_token)->first();
                if (!$is_user_token_exists) {
                    $user_device_token = new UserDeviceToken();
                    $user_device_token->user_id = $user->id;
                    $user_device_token->device_type = $request->device_type;
                    $user_device_token->device_token = $request->device_token;
                    $user_device_token->save();
                }
            }

            $user_model = new User();
            $data = $user_model->getSingleUserData($user);
            $data['access_token'] = $tokenResult->plainTextToken;
            $data['token_type'] = 'Bearer';

            // $data = $this->getUserData($user);
            return $this->sendResponse(__('Sign up successfully.'), $data);
        } else {
            return $this->sendError(__('User does not saved'));
        }
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            // 'phone_number' => 'required|string',
            'password' => 'required|string',
            'remember_me' => 'boolean',
            'device_type' => 'required|string',
            'device_token' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->sendValidationError(__('Validation Error'), $validator->errors());
        }

        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return $this->sendError(__('Not valid credentials.'));
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Aspire Access Token');


        $is_user_token_exists = UserDeviceToken::where('user_id', $user->id)->where('device_token', $request->device_token)->first();
        if (!$is_user_token_exists) {
            $user_device_token = new UserDeviceToken();
            $user_device_token->user_id = $user->id;
            $user_device_token->device_type = $request->device_type;
            $user_device_token->device_token = $request->device_token;
            $user_device_token->save();
        }

        $user_model = new User();
        $data = $user_model->getSingleUserData($user);
        $data['access_token'] = $tokenResult->plainTextToken;
        $data['token_type'] = 'Bearer';

        return $this->sendResponse(__('Logged in successfully.'), $data);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'device_type' => 'required|string',
            'device_token' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->sendValidationError(__('Validation Error'), $validator->errors());
        }

        try {
            $user = $request->user();

//            $request->user()->token()->revoke();
            $request->user()->currentAccessToken()->delete();


            $user_device_token = UserDeviceToken::where('device_token', $request->device_token)->where('user_id', $user->id)->first();

            if ($user_device_token) {
                $user_device_token->delete();
            }

            $data = [];
            return $this->sendResponse(__('User logged out successfully.'), $data);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function updateProfile(Request $request)
    {
        $user = $request->user();

        if (!$user) {
            return $this->sendError(__('No user found.'));
        }

        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email|unique:users,email,' . $user['id'],

        ]);

        if ($validator->fails()) {
            return $this->sendValidationError(__('Validation Error'), $validator->errors());
        }

        if ($validator->fails()) {
            return $this->sendValidationError(__('Validation Error'), $validator->errors());
        }

        if (isset($request->first_name)) {
            $user->first_name = $request->first_name;
        }

        if (isset($request->last_name)) {
            $user->last_name = $request->last_name;
        }

        if (isset($request->email)) {
            $user->email = $request->email;
        }

        if (isset($request->country_code)) {
            $user->country_code = $request->country_code;
        }

        if (isset($request->phone)) {
            $user->phone = $request->phone;
        }


        try {
            if ($user->save()) {
                $user_model = new User();
                $data = $user_model->getSingleUserData($user);
                return $this->sendResponse(__('Profile updated successfully.'), $data);
            } else {
                return $this->sendError(__('User does not saved'));
            }
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|confirmed',
            'password_confirmation' => 'required|string',
            'current_password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->sendValidationError(__('Validation Error'), $validator->errors());
        }

        $user = $request->user();
        if (!Hash::check($request->current_password, $user->password)) {
            return $this->sendError(__('Current password is wrong.'));
        }
        $user->password = Hash::make($request->password);

        if ($user->save()) {
            $user_model = new User();
            $data = $user_model->getSingleUserData($user);
            return $this->sendResponse(__('Password changed successfully.'), $data);
        } else {
            return $this->sendError(__('Password does not changed.'));
        }
    }

    public function getUserProfileData(Request $request)
    {
        $user = $request->user();
        if ($user) {
            $user_model = new User();
            $data = $user_model->getSingleUserData($user);
            return $this->sendResponse(__('User profile data listed successfully.'), $data);
        }
        return $this->sendError(__('User does not exist.'));
    }
}
