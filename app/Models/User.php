<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'country_code',
        'phone',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function getIsAdminAttribute()
    {
        return $this->roles->contains(1);
    }

    public function getIsUserAttribute()
    {
        return $this->roles->contains(2);
    }
    
    public function getSingleUserData($user)
    {
        $user_data = [];
        if ($user->id) {
            $user_data['id'] = $user->id;
            $user_data['first_name'] = ($user->first_name) ?? '';
            $user_data['last_name'] = ($user->last_name) ?? '';
            $user_data['country_code'] = ($user->country_code) ?? '';
            $user_data['phone'] = ($user->phone) ?? '';
            $user_data['email'] = ($user->email) ?? '';
        }
        return $user_data;
    }
}
