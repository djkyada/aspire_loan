<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanRepayment extends Model
{
    use HasFactory;

    public function getSingleLoanRepaymentData($loan_repayment)
    {
        $loan_repayment_data = [];
        if ($loan_repayment->id) {
            $loan_repayment_data['id'] = $loan_repayment->id;
            $loan_repayment_data['loan_id'] = ($loan_repayment->loan_id) ?? 0;
            $loan_repayment_data['amount'] = ($loan_repayment->amount) ?? 0;
            $loan_repayment_data['interest'] = ($loan_repayment->interest) ?? 0;
            $loan_repayment_data['principal'] = ($loan_repayment->principal) ?? 0;
            $loan_repayment_data['payment_date'] = ($loan_repayment->payment_date) ?? '';
            $loan_repayment_data['notes'] = ($loan_repayment->notes) ?? '';
            $loan_repayment_data['created_at'] = ($loan_repayment->created_at) ? Carbon::parse($loan_repayment->created_at)->format('Y-m-d H:i:s') : '';
        }
        return $loan_repayment_data;
    }
}
