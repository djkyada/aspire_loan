<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanApplication extends Model
{
    use HasFactory;

    public function loan_repayments()
    {
        return $this->hasMany(LoanRepayment::class, 'loan_id', 'id');
    }

    public function getSingleLoanApplicationData($loan_application)
    {
        $loan_application_data = [];
        if ($loan_application->id) {
            $loan_application_data['id'] = $loan_application->id;
            $loan_application_data['amount'] = ($loan_application->amount) ?? 0;
            $loan_application_data['amount_paid'] = ($loan_application->amount_paid) ?? 0;
            $loan_application_data['emi'] = ($loan_application->emi) ?? 0;
            $loan_application_data['amount_remaining'] = ($loan_application->amount_remaining) ?? 0;
            $loan_application_data['total_amount_with_interest'] = ($loan_application->total_amount_with_interest) ?? 0;
            $loan_application_data['interest'] = ($loan_application->interest) ?? 0;
            $loan_application_data['duration'] = ($loan_application->duration) ?? 0;
            $loan_application_data['installments_total'] = ($loan_application->installments_total) ?? 0;
            $loan_application_data['installments_paid'] = ($loan_application->installments_paid) ?? 0;
            $loan_application_data['installments_remaining'] = ($loan_application->installments_remaining) ?? 0;
            $loan_application_data['date_applied'] = ($loan_application->date_applied) ?? 0;
            $loan_application_data['date_loan_ends'] = ($loan_application->date_loan_ends) ?? '';
            $loan_application_data['date_completed'] = ($loan_application->date_completed) ?? '';
            $status = '';
            switch ($loan_application->status) {
                case LOAN_STATUS_NEW:
                    $status = 'new';
                    break;
                case LOAN_STATUS_APPROVED:
                    $status = 'approved';
                    break;
                case LOAN_STATUS_REJECTED:
                    $status = 'rejected';
                    break;
                case LOAN_STATUS_COMPLETED:
                    $status = 'completed';
                    break;
                default:
                    break;
            }
            $loan_application_data['status'] = $status;
        }
        return $loan_application_data;
    }
}
