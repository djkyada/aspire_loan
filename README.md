# Aspire Loan Application

## About App

This is a custom web app designed for aspire. This is a test app, which includes basic functionality to manage loan
application.

### Features

- Made using laravel 8.
- Uses laravel passport for api authentication.
- Added a command to initialize app in one shot.
- Attached postman collection for api.

## Steps to install the web app

1. After clone or download the app, go to application folder using terminal or cmd and run this command **composer install**.
2. Create virtual domain to access this application. Here I have made domain named aspireloan.local and I can access that using http://aspireloan.local/
3. Rename .env.dev file in root with .env.
4. Change database and app url in **.env** file.
5. After that run this command to initialize the app **php artisan project:init**.
6. Import postman collection and environment variable attached in **postman collection** directory.
7. You need to replace **APP_URL** with your application's public url.
8. You will get one token in response of login api. You need to set that token as **ACCESS_TOKEN** in postman environment variable.

## Methods and variables used in app
- Added helper.php file which includes all the contants

## Generated users

#####

#### Admin user
    email: admin@admin.com
    password: password

#### End user
    email: dhananjaykyada@gmail.com
    password: password
###

### Contact Developer

- **[Dhananjay Kyada](dhananjaykyada@gmail.com)**
